const userController = require('../controllers/userController');

module.exports = (app) => {
	app.get('/', (req, res) => {
		res.render('index', { title: 'Catch the thieves' });
	});
	
	app.get('/signup', userController.userCreateGet);
	app.post('/signup', userController.postValidate,userController.userSignupPost);

	app.get('/signin', userController.userLoginGet);
	app.post('/signin', userController.postValidate, userController.userSigninPost);

	app.get('/logout', userController.userLogoutGet);
};
