'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		return await queryInterface.createTable('FriendRequests', {
			requesterId: {
				type: Sequelize.INTEGER(11),
				allowNull: false,
				references: {
					model: {
						tableName: 'User',
					},
					key: 'id'
				},
				onDelete: 'CASCADE'
			},
			requesteeId: {
				type: Sequelize.INTEGER(11),
				allowNull: false,
				references: {
					model: {
						tableName: 'User',
					},
					key: 'id'
				},
				onDelete: 'CASCADE'
			}
		});
	},

	down: async (queryInterface) => {
		return await queryInterface.dropTable('FriendsRequests');
	}
};