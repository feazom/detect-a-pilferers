module.exports = (sequelize, DataTypes) => {
	const RoundResults = sequelize.define('RoundResults', {
		id: {
			type: DataTypes.INTEGER(11),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false
		},
		// Winning team, false is a victory for thieves, otherwis the catchers win
		winningTeam: {
			type: DataTypes.BOOLEAN,
			allowNull: false
		},
		endTime: {
			type: DataTypes.DATE,
			allowNull: false
		}
	});

	RoundResults.associate = (models) => {
		// many-to-many associations for the list of users who participated in the round
		RoundResults.belongsToMany(models.User, { through: 'UserResults' });				
	};
	return RoundResults;
};