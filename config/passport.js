const crypto = require('crypto');
const User = require('../db/models/index').User;

const localStrategy = require('passport-local').Strategy;

module.exports = (passport) => {
	passport.use('local-signup', new localStrategy({
		usernameField: 'nickname',
		passwordField: 'password'
	}, function (req, nickname, password, done) {
		User.findOne({
			where: { nickname: nickname }
		}).then((user) => {
			// need logs here
			if (user) {
				return done(null, false, {
					message: 'This nickname is already exist'
				});
			} else {
				User.create({
					nickname: nickname,
					password: password
				}).then((newUser) => {
					if (!newUser) {
						return done(null, false);
					}
					if (newUser) {
						return done(null, newUser);
					}
				});
			}
		});
	}));

	passport.use('local-signin', new localStrategy({
		usernameField: 'nickname',
		passwordField: 'password'
	},
	function (req, nickname, password, done) {
		const isValidPassword = function(password, expectedPassword) {
			return encryptPassword(nickname, password) === expectedPassword;
		};

		User.findOne({
			where: {
				nickname: nickname
			}
		}).then((user) => {
			if (!user) {
				return done(null, false, {
					message: 'Nickname does not exist'
				});
			}

			if (!isValidPassword(password, user.password)) {
				return done(null, false, {
					message: 'Incorrect password'
				});
			}

			const userInfo = user.get();
			return done(null, userInfo);

		}).catch((err) => {
			// logs here
			return done(null, false, {
				message: 'Something went wrong'
			});
		});
	}));

	passport.serializeUser((user, done) => {
		done(null, user.id);
	});

	passport.deserializeUser((id, done) => {
		User.findById(id).then((user) => {
			if(user) {
				done(null, user.get());
			} else {
				done(user.errors, null);
			}
		});
	});
};

function encryptPassword(password,salt) {
	return crypto.createHmac('sha256', salt).update(password).digest('hex');
}